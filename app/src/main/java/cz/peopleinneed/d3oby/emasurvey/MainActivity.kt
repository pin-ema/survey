package cz.peopleinneed.d3oby.emasurvey

import android.annotation.SuppressLint
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.webkit.ConsoleMessage
import android.webkit.WebChromeClient
import android.webkit.WebView

class MainActivity : AppCompatActivity() {

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.main_activity)

        val webView: WebView = findViewById(R.id.webView)
        webView.settings.javaScriptEnabled = true
        webView.settings.allowUniversalAccessFromFileURLs = true


        webView.webChromeClient = object : WebChromeClient() {
            override fun onConsoleMessage(consoleMessage: ConsoleMessage?): Boolean {
                consoleMessage?.apply {
                    Log.d("WEB_APP", "[${messageLevel()}] ${message()}")
                }
                return true
            }
        }
    }

    @SuppressLint("JavascriptInterface")

    var bindings: JsBindings? = null

    override fun onResume() {
        super.onResume()

        val webView: WebView = findViewById(R.id.webView)
        bindings = JsBindings(this)
        webView.addJavascriptInterface(bindings, "EMA_NATIVE")
        webView.loadUrl("file:///android_asset/app.html")
    }

    override fun onBackPressed() {
        bindings?.navigateBack {
            if (it) super.onBackPressed()
        }
    }

    override fun onPause() {
        super.onPause()
        bindings?.saveAppState()
    }

}
