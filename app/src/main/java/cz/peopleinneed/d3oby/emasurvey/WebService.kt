package cz.peopleinneed.d3oby.emasurvey

import android.os.AsyncTask
import android.util.Log
import org.json.JSONObject
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call

interface WebService {

    @retrofit2.http.Headers("Content-Type: application/json")
    @retrofit2.http.POST("android_api/login")
    fun logIn(@retrofit2.http.Body body: RequestBody): Call<ResponseBody>

    @retrofit2.http.Headers("Content-Type: application/json")
    @retrofit2.http.POST("android_api/logout")
    fun logOut(@retrofit2.http.Body body: RequestBody): Call<ResponseBody>

    @retrofit2.http.Headers("Content-Type: application/json")
    @retrofit2.http.POST("android_api/upload")
    fun uploadFinalized(@retrofit2.http.Body body: RequestBody): Call<ResponseBody>
}

class WebServiceCaller(private val request: Call<ResponseBody>) {

    fun call(callback: (result: JSONObject) -> Unit) {
        request.enqueue(object: retrofit2.Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, rawResponse: retrofit2.Response<ResponseBody>) {
                val code = rawResponse.code()
                if (code == 200) {
                    executeCallback(
                            callback,
                            createBodyResponse(rawResponse.body())
                    )

                } else {
                    executeCallback(
                            callback,
                            createFailResponse()
                    )

                }
            }

            override fun onFailure(call: Call<ResponseBody>, throwable: Throwable) {
                executeCallback(
                        callback,
                        createFailResponse()
                )
            }
        }
        )
    }

    private fun executeCallback(callback: (result: JSONObject) -> Unit, data: JSONObject) {
        AsyncTask.execute {
            callback(data)
        }
    }

    private fun createBodyResponse(body: ResponseBody?): JSONObject {
        return if (body == null) {
            val result = JSONObject()
            result.put("empty", true)
            result

        } else {
            try {
                JSONObject(body.string())
            } catch (e: org.json.JSONException) {
                createFailResponse()
            }

        }
    }

    private fun createFailResponse(): JSONObject {
        val result = JSONObject()
        result.put("ok", false)
        return result
    }

}
