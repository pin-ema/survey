package cz.peopleinneed.d3oby.emasurvey

import android.arch.persistence.room.Room
import android.os.AsyncTask
import android.preference.PreferenceManager
import android.util.Log
import android.webkit.JavascriptInterface
import android.webkit.ValueCallback
import android.webkit.WebView
import org.json.JSONArray
import org.json.JSONObject

class JsBindings(private val activity: MainActivity) {

    private val preferences = PreferenceManager.getDefaultSharedPreferences(
            activity
    )

    private val db = Room.databaseBuilder(
            activity.applicationContext,
            AppDatabase::class.java, "ema-db"
    ).
            fallbackToDestructiveMigration().
            build()
    private val dbDao = db.dao()

    private val api = retrofit2.Retrofit.Builder()
//            .baseUrl("http://10.0.2.2:3000/") // android device subnet addressing -> host loopback
            .baseUrl("https://www.ema-monitoring.org/")
            .build()
            .create(WebService::class.java)

    private var gps: GPSReader? = null

    @JavascriptInterface
    fun getInitState(): String {
//        dbDao.deleteAllForms()
        val state = JSONObject()

        var lang: String = preferences.getString("lang", null) ?: ""
        if (lang == "") {
            lang = java.util.Locale.getDefault().language
            if (!arrayOf("en", "pt").contains(lang)) lang = "en"
            setLocale(lang)
        }
        state.put("language", lang)

        state.put("user_login", dbDao.getEntry("user_login"))
        state.put("user_name", dbDao
                .getEntry("user_name"))
        state.put("user_region", dbDao.getEntry("user_region"))

//        dbDao.putEntry(Entry("persisted_state","{}"))
        val persistedState: String? = dbDao.getEntry("persisted_state")
        if (persistedState != null) state.put("persisted_state", JSONObject(persistedState))

        state.put("ok", true)
        return state.toString()
    }

    @JavascriptInterface
    fun setLocale(lang: String) {
        with (preferences.edit()) {
            putString("lang", lang)
            commit()
        }
    }

    @JavascriptInterface
    fun getUserData(): String {
        val user = dbDao.getEntry("user_login")
        val json = JSONObject()

        val unfinished = dbDao.getUnfinishedForms(user).map{ attrs -> JSONObject(attrs) }
        json.put("unfinished", JSONArray(unfinished))

        val finalized = dbDao.getFinalizedForms(user).map{ attrs -> JSONObject(attrs) }
        json.put("finalized", JSONArray(finalized))

        json.put("to_upload_count", dbDao.getFormForUploadCount(user))
        return json.toString()
    }

    @JavascriptInterface
    fun logIn(payload: String) {
        val body = okhttp3.RequestBody.create(
                okhttp3.MediaType.parse("application/json; charset=utf-8"),
                payload
        )
        val caller = WebServiceCaller(api.logIn(body))
        caller.call { json: JSONObject ->
            if (json.optBoolean("ok")) {
                dbDao.putEntry(Entry(
                        "api_token",
                        json.getString("token")
                ))
                dbDao.putEntry(Entry(
                        "user_login",
                        json.getString("login")
                ))
                dbDao.putEntry(Entry(
                        "user_name",
                        json.getString("name")
                ))
                dbDao.putEntry(Entry(
                        "user_region",
                        (
                                if (json.isNull("onlyRegion")) ""
                                else json.getString("onlyRegion")
                                )
                ))
                invokeJSCallback(getInitState())

            } else {
                invokeJSCallback(json.toString())

            }
        }
    }

    @JavascriptInterface
    fun logOut() {
        val body = okhttp3.RequestBody.create(
                okhttp3.MediaType.parse("application/json; charset=utf-8"),
                createApiPayload().toString()
        )
        val caller = WebServiceCaller(api.logOut(body))
        caller.call { json: JSONObject ->
            if (json.optBoolean("ok")) {
                dbDao.deleteEntry("api_token")
                dbDao.deleteEntry("user_login")
            }

            invokeJSCallback(json.toString())
        }
    }

    @JavascriptInterface
    fun openForm(id: String): String {
        val formId = id.toLongOrNull()
        if (formId != null) return dbDao.getForm(formId)
        else return "null"
    }

    @JavascriptInterface
    fun saveForm(attrs_payload: String) {
        createOrUpdateForm(attrs_payload)
    }

    @JavascriptInterface
    fun deleteForm(attrs_payload: String) {
        val attrs = JSONObject(attrs_payload)
        val formId = attrs.getString("id").toLong()
        dbDao.deleteForm(formId);
    }

    @JavascriptInterface
    fun finalizeForm(attrs_payload: String) {
        val formId = createOrUpdateForm(attrs_payload)
        if (formId != null) dbDao.setFormFinalized(formId)
    }

    @JavascriptInterface
    fun readGPS() {
        gps = GPSReader(activity)
        gps!!.start { json ->
            invokeJSCallback(json.toString())
            gps = null
        }
    }

    @JavascriptInterface
    fun cancelGps() {
        gps?.cancel()
        gps = null
    }

    @JavascriptInterface
    fun uploadFinalized() {
        val user = dbDao.getEntry("user_login")

        val forms = dbDao.getFinalizedForms(user).map{ attrs -> JSONObject(attrs) }
        val payload = createApiPayload()
        payload.put("forms", JSONArray(forms))

        val body = okhttp3.RequestBody.create(
                okhttp3.MediaType.parse("application/json; charset=utf-8"),
                payload.toString()
        )
        val caller = WebServiceCaller(api.uploadFinalized(body))
        caller.call { json: JSONObject ->
            if (json.optBoolean("ok")) {
                forms.forEach{ attrs_json ->
                    val formId = attrs_json.getLong("id")
                    dbDao.setFormUploaded(formId)
                }
                invokeJSCallback("{\"ok\": true}")

            }
            else {
                invokeJSCallback(json.toString())

            }
        }
    }

    private fun createOrUpdateForm(attrs_payload: String): Long? {
        val user = dbDao.getEntry("user_login")

        val attrs = JSONObject(attrs_payload)
        var formId: Long = 0

        if (!attrs.has("id")) {
            formId = dbDao.putForm(Form(formId, user, false, false, ""))
            attrs.put("id", formId.toString())

        } else {
            formId = attrs.getString("id").toLong()

        }

        dbDao.updateForm(formId, attrs.toString())

        return formId
    }

    private fun createApiPayload(): JSONObject {
        val json = JSONObject()
        json.put(
                "token",
                dbDao.getEntry("api_token")
        )
        return json
    }

    private fun invokeJSCallback(payload: String) {
        val escaped = JSONObject.quote(payload)
        activity.runOnUiThread {
            val webView: WebView = activity.findViewById(R.id.webView)
            webView.evaluateJavascript("EMA_NATIVE.callback($escaped)", null)
        }
    }

    private fun evaluateJSCall(command: String, callback: ValueCallback<String>) {
        activity.runOnUiThread {
            val webView: WebView = activity.findViewById(R.id.webView)
            webView.evaluateJavascript(command, callback)
        }
    }

    fun saveAppState() {
        evaluateJSCall("EMA.stateToPersist()", ValueCallback {
            val result = it
            AsyncTask.execute {
                dbDao.putEntry(Entry(
                        "persisted_state",
                        result
                ))
            }
        })
    }

    fun navigateBack(callback: (result: Boolean) -> Unit) {
        evaluateJSCall("EMA.navigateBack()", ValueCallback {
            val response = JSONObject(it)
            callback(response.optString("ignored") == "1")
        })
    }

}
