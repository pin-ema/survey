package cz.peopleinneed.d3oby.emasurvey

import android.Manifest
import android.content.Context.LOCATION_SERVICE
import android.content.pm.PackageManager
import android.location.Criteria
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import org.json.JSONObject

class GPSReader (private val activity: MainActivity) : LocationListener {

    private val manager = activity.getSystemService(LOCATION_SERVICE) as LocationManager

    private val result = JSONObject()

    var callback : ((JSONObject) -> Unit)? = null

    fun start (on_finished: (JSONObject) -> Unit) {
        callback = on_finished

        val permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION)
        if (permission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    activity,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    1
            )
            this.failWithReason("denied")

        } else {
            val criteria = Criteria()
            criteria.accuracy = Criteria.ACCURACY_FINE
            manager.requestSingleUpdate(criteria, this, null)
        }
    }

    fun cancel () {
        if (callback != null) {
            callback = null
            manager.removeUpdates(this)
        }
    }

    private fun failWithReason (reason: String?) {
        result.put("fail", true)
        if (reason != null) result.put("reason", reason)
        callback?.invoke(result)
    }

    override fun onProviderDisabled(provider: String?) {
        this.failWithReason("denied")
    }

    override fun onLocationChanged(location: Location?) {
        if (location == null) {
            this.failWithReason(null)

        } else {
            result.put("ok", true)
            val value = JSONObject()
            value.put("t", location.latitude)
            value.put("n", location.longitude)
            result.put("value", value)
            callback?.invoke(result)

        }
    }

    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {}

    override fun onProviderEnabled(provider: String?) {}

}

