package cz.peopleinneed.d3oby.emasurvey
import android.arch.persistence.room.*

@Entity
data class Entry(
        @PrimaryKey var name: String,
        @ColumnInfo var value: String
)

@Entity
data class Form(
        @PrimaryKey(autoGenerate = true) var uid: Long,
        @ColumnInfo var user: String,
        @ColumnInfo var finalized: Boolean,
        @ColumnInfo var uploaded: Boolean,
        @ColumnInfo var json_data: String
)

@Dao
interface DbDao {

    /// ENTRY
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun putEntry(entry: Entry)

    @Query("SELECT value FROM entry WHERE name = :name")
    fun getEntry(name: String): String

    @Query("DELETE FROM entry WHERE name = :name")
    fun deleteEntry(name: String)

    /// Form
    @Insert
    fun putForm(form: Form): Long

    @Query("SELECT json_data FROM form WHERE uid = :id")
    fun getForm(id: Long): String

    @Query("UPDATE form SET json_data = :json WHERE uid = :id;")
    fun updateForm(id: Long, json: String)

    @Query("DELETE FROM form WHERE uid = :id")
    fun deleteForm(id: Long)

    @Query("DELETE FROM form;")
    fun deleteAllForms()

    @Query("UPDATE form SET finalized = 1 WHERE uid = :id;")
    fun setFormFinalized(id: Long)

    @Query("UPDATE form SET uploaded = 1 WHERE uid = :id")
    fun setFormUploaded(id: Long)

    @Query("SELECT json_data FROM form WHERE user = :user AND NOT finalized;")
    fun getUnfinishedForms(user: String): List<String>

    @Query("SELECT json_data FROM form WHERE user = :user AND finalized;")
    fun getFinalizedForms(user: String): List<String>

    @Query("SELECT json_data FROM form WHERE user = :user AND finalized AND NOT uploaded;")
    fun getFormForUpload(user: String): List<String>

    @Query("SELECT COUNT(uid) FROM form WHERE user = :user AND finalized AND NOT uploaded;")
    fun getFormForUploadCount(user: String): Int

}

@Database(entities = [Entry::class, Form::class], version = 3)
abstract class AppDatabase : RoomDatabase() {
    abstract fun dao(): DbDao

//    companion object {
//        fun migration_1_2(): Migration {
//            return object: Migration(1, 2) {
//                override fun migrate(database: SupportSQLiteDatabase) {
//                    database.execSQL("DELETE FROM form;")
//                    database.execSQL("ALTER TABLE form ADD COLUMN uploaded INTEGER NOT NULL DEFAULT 0")
//
//                }
//            }
//        }
//    }

}
