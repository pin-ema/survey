import React from 'react';
import {connect} from 'react-redux';

import PageLayout from '../page_layout';
import ema from "../../ema";
import * as icons from '../icons';

class HomePage extends React.Component {

    render () {
        const {t, user, openSettings} = this.props;
        const top_bar = {
            t,
            no_logo: !user,
            openSettings
        };
        return <PageLayout
            bar={top_bar}>
            {user ?
                this.renderHomePage() :
                this.renderWelcome()
            }
        </PageLayout>;
    }

    renderHomePage () {
        const { t, user, homePageAction } = this.props;

        const { unfinished, finalized, to_upload_count } = user.data || {};

        const actions = [
            {
                action: 'unfinished',
                type: 'primary',
                icon: icons.ClipboardIcon,
                number: unfinished ? unfinished.length : undefined,
            },

            {
                action: 'new',
                type: 'success',
                icon: icons.ClipboardIcon,
            },

            {
                action: 'browse',
                type: 'primary',
                icon: icons.SearchIcon,
                number: finalized && finalized.length,
            },

            {
                action: 'upload',
                type: to_upload_count ? 'success' : 'secondary',
                icon: icons.UploadIcon,
                number: to_upload_count || undefined,
            },

        ];

        return <div
            className="ema-body">

            <h2
                className="text-center">
                <strong>
                    {t('home.name')}
                </strong>
            </h2>

            <div
                className="row no-gutters align-items-center text-center mt-3 mb-3">
                <div
                    className="col-3 text-right">
                    {t('user.logged_in_as')}:
                </div>
                <div
                    className="col-9 ema-header text-center">
                    {user.name}
                </div>
            </div>

            <div
                className="home-page-actions">
                {actions.map(({ action, type, icon, number }) => {
                    const className = `btn btn-${type || 'primary'} btn-action`;
                    const handler = () => homePageAction(action);
                    return <div
                        key={action}>
                        <div
                            className={className}
                            onClick={handler}>
                            {React.createElement(icon)}
                            <div
                                className="ml-2 flex-fill">
                                {t(`home.action.${action}`)}
                                {number !== undefined && [
                                    <br key="br"/>,
                                    <span key="num">{number}</span>
                                ]}
                            </div>
                        </div>
                    </div>;
                })}
            </div>

            <div
                className="text-center ema-pin-logo mt-3">
                <img
                    src="build/pin.png"/>
            </div>

        </div>;
    }

    renderWelcome () {
        const {t, openSettings} = this.props;
        return <div
            className="ema-body -white">

            <div
                className="text-center ema-pin-logo">
                <img
                    src="build/pin.png"/>
            </div>

            <h3
                className="text-center mb-2 text-body">
                {t('home.welcome_title')}
            </h3>

            <div
                className="text-center ema-logo mb-2">
                <img
                    src={`build/big_logo.png`}/>
            </div>

            <h3
                className="text-center mb-5 text-body">
                {t('home.name')}
            </h3>

            <div
                className="text-center">
                <button
                    className="btn btn-primary"
                    onClick={openSettings}>
                    {t('home.welcome_hint')}
                </button>
            </div>
        </div>;
    }

}

function state ({user}) {
    return {
        user,
    };
}

function dispatch (dispatch) {
    return {
        openSettings () {
            dispatch({
                type: 'GOTO_SETTINGS_PAGE'
            });
        },

        homePageAction (action) {
            switch (action) {
                case 'browse':
                    dispatch({
                        type: 'GOTO_BROWSE_PAGE',
                        list: 'finalized',
                    });
                    break;

                case 'new':
                    dispatch({
                        type: 'OPEN_FORM',
                        form: {
                            editable: true,
                            attributes: null,
                            return_action: {
                                type: 'GOTO_HOME_PAGE'
                            },
                        }
                    });
                    break;

                case 'unfinished':
                    dispatch({
                        type: 'GOTO_BROWSE_PAGE',
                        list: 'unfinished',
                    });
                    break;

                case 'upload':
                    ema.invokeAction('uploadForms');
                    break;
            }
        },

    };
}

const ConnectedHomePage = connect(
    state,
    dispatch,
)(
    HomePage
);
export default ConnectedHomePage;
