import React from 'react';
import {connect} from 'react-redux';

import ema from '../../ema';
import utils from '../../utils';
import { generateThumbnail } from '../../reducers/structs/user';
import PageLayout from "../page_layout";

class BrowsePage extends React.Component {

    // constructor (props) {
    //     super(props);
    // }

    render  () {
        const {t, list, leave} = this.props;
        const top_bar = {
            t,
            caption: t('browse.caption.' + list),
            goBack: leave
        };
        return <PageLayout
            bar={top_bar}>
            <div
                className="ema-body">
                {this.renderPage()}
            </div>
        </PageLayout>;
    }

    renderPage () {
        const { t, list, openForm } = this.props;
        return <div>
            <ul
                className="list-group">
                {this.props.thumbnails.map(item =>
                    <li
                        key={item.id}
                        data-id={item.id}
                        className="list-group-item row no-gutters"
                        onClick={openForm}>
                        <div
                            className="col-12">
                            <small
                                className="float-right">
                                {utils.fTime(item.time)}
                            </small>
                        </div>

                        <small
                            className="col-12 text-muted">
                            {t('home.unfinished.teacher')}:
                        </small>
                        <div
                            className="col-12">
                            {item.teacher}
                        </div>

                        <small
                            className="col-12 text-muted">
                            {t('home.unfinished.school')}:
                        </small>
                        <div
                            className="col-12">
                            {item.school}
                            <br/>
                            {item.address}
                        </div>
                    </li>
                )}
            </ul>
        </div>;
    }

}

function state ({ user, lang, list }) {
    const data = user.data || {};
    let forms;

    if (list === 'unfinished') {
        forms = data.unfinished;
    } else {
        forms = data.finalized;
    }

    if (!forms) forms = [];
    return {
        list,
        forms,
        thumbnails: forms.map(attrs => generateThumbnail(attrs, lang)),
    };
}

function dispatch (dispatch) {
    return {
        doOpenFrom (attributes, editable, return_action) {
            dispatch({
                type: 'OPEN_FORM',
                form: {
                    editable,
                    return_action,
                    attributes,
                }
            });
        },
        leave () {
            dispatch({
                type: 'GOTO_HOME_PAGE'
            });
        },
    };
}

function mergeProps (state, dispatch, own) {
    return {
        ...own,
        ...state,
        ...dispatch,

        openForm (e) {
            const attrs = ema.invokeAction(
                'getFormAttrs',
                e.currentTarget.dataset.id
            );
            dispatch.doOpenFrom(
                attrs,
                state.list === 'unfinished',
                {
                    type: 'GOTO_BROWSE_PAGE',
                    list: state.list
                }
            )
        }
    };
}

const ConnectedBrowsePage = connect(
    state,
    dispatch,
    mergeProps
)(
    BrowsePage
);
export default ConnectedBrowsePage;
