import React from 'react';
import {Provider, connect} from 'react-redux';
import ema from '../ema';
import utils from '../utils';
import StackTrace from 'stacktrace-js';

import HomePage from './home_page/home_page';
import SettingsPage from './settings_page/settings_page';
import FormPage from './form_page/form_page';
import FormSectionPage from './form_section_page/form_section_page';
import BrowsePage from './browse_page/browse_page';

const pages_components = {
    HomePage,
    SettingsPage,
    FormPage,
    FormSectionPage,
    BrowsePage
};

export default class App extends React.Component {

    constructor (props) {
        super(props);
        this.state = {
            error: null,
        };
        this.reInit = this.reInit.bind(this);
    }

    componentDidUpdate (prevProps) {
        const { error } = this.props;
        if (error !== prevProps.error) {
            this.processAndSetError(error);
        }
    }

    componentDidCatch (error) {
        this.processAndSetError(error);
    }

    processAndSetError(error) {
        let origStack = [];
        try {
            origStack = error.stack.split("\n");
            origStack = origStack.slice(1, 5);
        } catch {}
        this.setState({
            error: {
                message: error.toString(),
                stack: origStack.map(line => ({
                    call: line,
                })),
            }
        });

        StackTrace.fromError(error).then((stackFrames) => {
            stackFrames = stackFrames.slice(0, 5);
            this.setState({
                error: {
                    message: error.toString(),
                    stack: stackFrames.map(frame => {
                        const { fileName, lineNumber, columnNumber} = frame;
                        return {
                            call: frame.functionName || '-',
                            source: fileName + ' ' + lineNumber + ':' + columnNumber,
                        }
                    }),
                }
            });
        });
    }

    reInit() {
        ema.invokeAction('init');
        this.setState({
            error: null,
        });
    }

    render () {
        const { error } = this.state;
        const {page_id, lang} = this.props;
        const t = (key) => utils.t(key, lang);
        t.locale = lang || 'en';

        if (error) {
            return <div
                className="alert alert-danger m-3">
                <div
                    className="text-center"
                    onClick={this.reInit}>
                    <h2>
                        {t('app.error')}
                    </h2>
                    <br/>
                    <button
                        className="btn btn-sm btn-block">
                        {t('app.error_recovery')}
                    </button>
                </div>
                <hr/>
                <div
                    className="mb-3">
                    {error.message}
                </div>
                <div>
                    {error.stack.map(({ call, source }) => {
                        return <React.Fragment>
                            <div className="font-italic">
                                {call}
                            </div>
                            <div className="text-right text-muted">
                                {source}
                            </div>
                        </React.Fragment>;
                    })}
                </div>
            </div>;

        } else {
            const page = pages_components[page_id];
            if (page) {
                return  React.createElement(
                    page,
                    {t}
                );

            } else {
                return <div
                    className="alert alert-secondary m-3">
                    {t('app.loading')}
                </div>;
            }
        }
    }

}

const ConnectedApp = connect(
    ({page_id, lang, error}) => ({
        page_id,
        lang,
        error,
    })
)(App);

App.render_view = function (store) {
    return <Provider store={store}>
        <ConnectedApp />
    </Provider>;
};
