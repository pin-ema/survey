import CaretDownIcon from '@fortawesome/fontawesome-free/svgs/solid/caret-down.svg';
import CaretUpIcon from '@fortawesome/fontawesome-free/svgs/solid/caret-up.svg';
import SearchIcon from '@fortawesome/fontawesome-free/svgs/solid/search.svg';
import CheckedIcon from '@fortawesome/fontawesome-free/svgs/regular/check-square.svg';
import NonCheckedIcon from '@fortawesome/fontawesome-free/svgs/regular/square.svg';
import LocationIcon from '@fortawesome/fontawesome-free/svgs/solid/map-marker-alt.svg';
import CloseIcon from '@fortawesome/fontawesome-free/svgs/solid/times.svg';
import ClipboardIcon from '@fortawesome/fontawesome-free/svgs/solid/clipboard-list.svg';
import UploadIcon from '@fortawesome/fontawesome-free/svgs/solid/cloud-upload-alt.svg';

export {
    CaretDownIcon,
    CaretUpIcon,
    SearchIcon,
    CheckedIcon,
    NonCheckedIcon,
    LocationIcon,
    CloseIcon,
    ClipboardIcon,
    UploadIcon,
};
