import React, { useState } from 'react';
import {connect} from 'react-redux';
import ema from '../../ema';
import utils from '../../utils';

function state ({lang, modal}) {
    return {
        lang,
        form_id: modal.id
    };
}

function dispatch (dispatch) {
    return {
        hideModal () {
            dispatch({
                type: 'HIDE_MODAL'
            });
        }
    };
}

function mergeProps (state, dispatch, own) {
    return {
        ...own,
        ...state,
        ...dispatch,

        doDelete () {
            ema.invokeAction(
                'deleteFormAndLeave',
                state.form_id
            );
            ema.invokeAction('goHome');
        }
    };
}

function DeleteForm ({lang, hideModal, doDelete}) {
    const [ btnDisabled, setButtonDisabled ] = useState(false);
    return <div
        className="modal-body">
        <div
            className="alert alert-danger">
            {utils.t('form.actions.delete_warning', lang)}
        </div>

        <div
            className="mt-3 clearfix">
            <button
                className="btn btn-secondary float-left"
                onClick={hideModal}>
                {utils.t('app.cancel', lang)}
            </button>
            <button
                className="btn btn-danger float-right"
                onClick={() => {
                    setButtonDisabled(true);
                    doDelete();
                }}
                disabled={btnDisabled}>
                {utils.t('form.actions.delete', lang)}
            </button>
        </div>
    </div>
}

const ConnectedDeleteForm = connect(
    state,
    dispatch,
    mergeProps
)(
    DeleteForm
);
export default ConnectedDeleteForm;