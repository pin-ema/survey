import React from 'react';
import {connect} from 'react-redux';
import utils from '../../utils';

function state ({lang, modal}) {
    return {
        lang,
        message: modal.message,
        alert: modal.alert,
    };
}

function dispatch (dispatch) {
    return {
        close () {
            dispatch({
                type: 'HIDE_MODAL'
            });
        }
    };
}

function Info ({lang, close, message, alert}) {
    return <div
        className="modal-body">
        <div
            className={`alert alert-${alert}`}>
            {utils.t(message, lang)}
        </div>
        <div
            className="mt-3 text-center">
            <button
                className="btn btn-secondary"
                onClick={close}>
                {utils.t('app.close', lang)}
            </button>
        </div>
    </div>
}

const ConnectedInfo = connect(
    state,
    dispatch
)(
    Info
);
export default ConnectedInfo;