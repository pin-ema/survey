import React from 'react';
import {connect} from 'react-redux';
import ema from '../../ema';
import utils from '../../utils';

function state ({lang}) {
    return {
        lang
    };
}

function dispatch (dispatch) {
    return {
        logOut () {
            ema.invokeAction('logOut');
        },

        cancel () {
            dispatch({
                type: 'HIDE_MODAL'
            });
        }
    };
}

function LogOut ({lang, cancel, logOut}) {
    return <div
        className="modal-body">
        <div
            className="alert alert-warning">
            {utils.t('settings.log_out_warning', lang)}

            <div
                className="mt-3 clearfix">
                <button
                    className="btn btn-secondary float-left"
                    onClick={cancel}>
                    {utils.t('app.cancel', lang)}
                </button>
                <button
                    className="btn btn-danger float-right"
                    onClick={logOut}>
                    {utils.t('settings.log_out', lang)}
                </button>
            </div>
        </div>
    </div>
}

const ConnectedLogOut = connect(
    state,
    dispatch
)(
    LogOut
);
export default ConnectedLogOut;