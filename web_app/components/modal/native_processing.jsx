import React from 'react';
import {connect} from 'react-redux';
import ema from '../../ema';
import utils from '../../utils';

class NativeProcessing extends React.Component {

    render () {
        return <div
            className="modal-body">
            <div className="ema-spinner">
                <div className="-r1"/>
                <div className="-r2"/>
                <div className="-r3"/>
                <div className="-r4"/>
                <div className="-r5"/>
            </div>
            {this.renderContent()}
        </div>;
    }

    renderContent () {
        const {text, process} = this.props;
        switch (process) {
            case 'gps':
                return <div>
                    <div
                        className="text-center">
                        {text}
                    </div>
                    <div
                        className="text-right mt-2">
                        <button
                            className="btn btn-secondary"
                            onClick={this.props.cancelGps}>
                            {utils.t('app.cancel')}
                        </button>
                    </div>
                </div>;

            default:
                return <div
                    className="text-center">
                    {text}
                </div>;
        }
    }

}

const ConnectedNativeProcessing = connect(
    ({modal}) => {
        const {text, process} = modal;
        return {
            text,
            process
        };
    },

    (dispatch) => ({
        cancelGps () {
            ema.invokeAction('cancelGps');
        }
    })
)(NativeProcessing);


export default ConnectedNativeProcessing;