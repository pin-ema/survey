import React from 'react';
import { connect } from 'react-redux';
import * as icons from "../icons";
import utils from "../../utils";

// const CloseIcon = icons.CloseIcon;

class SelectOption extends React.Component {

    constructor (props) {
        super(props);
        this.state = {
            value: props.attributes[props.field_id],
            options: SelectOption.getOptions(props, ''),
            filtered: QUESTIONNAIRE.getField(props.field_id).control.filtered
        };

        this.handleFilterInput = this.handleFilterInput.bind(this);
        this.handleFilterInputDebounced = utils.debounce(
            this.handleFilterInputDebounced.bind(this),
            600
        );
    }

    render () {
        const { lang, field_id, putValue, hideModal } = this.props;
        const { value, filtered, options } = this.state;
        return <div
            className="modal-body">
            <div
                className="mb-4 d-flex align-items-center">
                <h3
                    className="m-0 flex-grow-1">
                    {utils.t(`form.q.attrs.${field_id}`, lang)}
                </h3>
                <button
                    className="btn"
                    onClick={hideModal}>
                    <icons.CloseIcon
                        fill="#9D9D9C"/>
                </button>
            </div>

            {filtered && <div
                className="input-group">
                <div
                    className="input-group-prepend">
                            <span
                                className="input-group-text">
                            <icons.SearchIcon
                                className="badge -icon"
                                fill="#9D9D9C"/>
                            </span>
                </div>
                <input
                    type="text"
                    className="form-control"
                    onInput={this.handleFilterInput}/>
            </div>}

            <ul
                className="list-group">
                {options.map(([val, text]) =>
                    <li
                        className={this.itemClassName(val === value)}
                        key={val}
                        onClick={() => putValue(field_id, val)}>
                        {this.renderItem(text)}
                    </li>
                )}
            </ul>
        </div>;
    }

    itemClassName (selected) {
        return selected ?
            'list-group-item -highlight' :
            'list-group-item';
    }

    renderItem (item) {
        if (typeof item === 'string') return item;
        return [
            <div
                key="caption">
                {item.caption}
            </div>,
            <small
                key="text">
                {item.text}
            </small>
        ];
    }

    handleFilterInput (e) {
        this.handleFilterInputDebounced(e.target.value);
    }

    handleFilterInputDebounced (filter) {
        this.setState({ options: SelectOption.getOptions(this.props, filter) });
    }

    static getOptions ({ field_id, attributes, lang }, filter) {
        let list = QUESTIONNAIRE.fields.getOptionsListFor(
            field_id, attributes, filter
        );
        list = list.getPairs(
            lang,
            key => utils.t(`form.q.opts.${field_id}.${key}`, lang)
        );
        // console.log(list);
        // list = list.map((key, translation) => {
        //     if (typeof translation === 'string' || !translation) return [ key, translation ];
        //     return [ key, '' ];
        // });
        if (list.length > 0 && list[0][0] === 'string') {
            list.sort((a, b) => {
                a = a[1];
                b = b[1];
                if (a < b) return -1;
                if (a > b) return 1;
                return 0;
            });
        }
        return list;
    }

}

const ConnectedSelectOption = connect(
    state => ({
        lang: state.lang,
        field_id: state.modal.field_id,
        attributes: state.form.attributes,
    }),

    dispatch => ({
        putValue (field_id, value) {
            dispatch({
                type: 'FORM_UPDATE_ATTR',
                id: field_id,
                value
            });
            dispatch({
                type: 'HIDE_MODAL'
            });
        },

        hideModal () {
            dispatch({
                type: 'HIDE_MODAL'
            });
        }

    })
)(
    SelectOption
);

export default ConnectedSelectOption;
