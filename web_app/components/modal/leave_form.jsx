import React from 'react';
import {connect} from 'react-redux';
import ema from '../../ema';
import utils from '../../utils';

import CloseIcon from '@fortawesome/fontawesome-free/svgs/solid/times.svg';

function state ({lang, form, modal}) {
    return {
        lang,
        attributes: form.attributes,
        return_action: modal.return_action
    };
}

function dispatch (dispatch) {
    return {
        hideModal () {
            dispatch({
                type: 'HIDE_MODAL'
            });
        },

        returnAction (action) {
            dispatch(action);
        },

    };
}

function mergeProps (state, dispatch, own) {
    return {
        ...own,
        ...state,
        ...dispatch,

        ignoreChanges () {
            dispatch.returnAction(state.return_action);
        },

        saveBeforeLeave () {
            ema.invokeAction(
                'saveForm',
                state.attributes
            );
            dispatch.returnAction(state.return_action);
        }
    };
}

function LeaveForm ({lang, hideModal, saveBeforeLeave, ignoreChanges}) {
    return <div
        className="modal-body">
        <div
            className="mb-4 text-right">
            <button
                className="btn"
                onClick={hideModal}>
                <CloseIcon
                    fill="#9D9D9C"/>
            </button>
        </div>

        <div
            className="text-center mb-3">
            <button
                className="btn btn-primary"
                onClick={saveBeforeLeave}>
                {utils.t('form.actions.save_changes', lang)}
            </button>
        </div>

        <div
            className="text-center">
            <button
                className="btn btn-secondary"
                onClick={ignoreChanges}>
                {utils.t('form.actions.discard_changes', lang)}
            </button>
        </div>
    </div>;
}

const ConnectedLeaveForm = connect(
    state,
    dispatch,
    mergeProps
)(
    LeaveForm
);
export default ConnectedLeaveForm;