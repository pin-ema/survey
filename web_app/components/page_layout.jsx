import React from 'react';
import {connect} from 'react-redux';
import jquery from 'jquery';

import TopBar from './top_bar';
import LogOut from './modal/log_out';
import LeaveForm from './modal/leave_form';
import DeleteForm from './modal/delete_form';
import NativeProcessing from './modal/native_processing';
import SelectOption from './modal/select_option';
import Info from './modal/info';

class PageLayout extends React.Component {

    render () {
        const {bar, modal, children} = this.props;

        return <div
            className="ema-page">
            {React.createElement(TopBar, bar)}
            {children}
            {modal && <Modal
                modal={modal}/>}
        </div>;
    }

}

const ConnectedPageLayout = connect(
    ({modal}) => ({
        modal
    })
)(PageLayout);

export default ConnectedPageLayout;

class Modal extends React.Component {

    render () {
        return <div
            className="modal"
            ref={el => this.$modal_root = jquery(el)}>
            <div
                className="modal-dialog modal-dialog-centered">
                <div
                    className="modal-content">
                    {this.renderBody()}
                </div>
            </div>
        </div>;
    }

    renderBody () {
        switch (this.props.modal.type) {
            case 'log_out':
                return <LogOut />;

            case 'leave_form':
                return <LeaveForm />;

            case 'delete_form':
                return <DeleteForm />;

            case 'native':
                return <NativeProcessing/>;

            case 'select_option':
                return <SelectOption/>;

            case 'info':
                return <Info/>;
        }
    }

    componentDidMount () {
        this.$modal_root.modal({
            backdrop: 'static',
            keyboard: false,
            show: true
        });
    }

    componentWillUnmount () {
        this.$modal_root.modal('hide');
    }

}


