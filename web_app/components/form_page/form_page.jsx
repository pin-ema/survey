import React from 'react';
import {connect} from 'react-redux';
import ema from '../../ema';
import utils from '../../utils';

import PageLayout from '../page_layout';
import CheckIcon from '@fortawesome/fontawesome-free/svgs/solid/check.svg';

class FormPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            finalizeButtonDisabled: false,
        };
        this.finalizeForm = this.finalizeForm.bind(this);
    }

    render () {
        const {t, editable, sections, completedSections} = this.props;
        const top_bar = {
            t,
            caption: t('form.caption'),
            goBack: this.props.leave
        };
        return <PageLayout
            bar={top_bar}>
            <div
                className="ema-body">
                <div
                    className="row no-gutters align-items-center mb-2">
                    <div
                        className="col-7">
                        <strong>
                            {t('form.q.attrs.created_at')}:
                        </strong>
                    </div>
                    <div
                        className="col-5">
                        {utils.fTime(this.props.attributes.created_at)}
                    </div>
                </div>

                {!editable && <div
                    className="row no-gutters align-items-center mb-2">
                    <div
                        className="col-7">
                        <strong>
                            {t('form.q.attrs.finalized_at')}:
                        </strong>
                    </div>
                    <div
                        className="col-5">
                        {utils.fTime(this.props.attributes.finalized_at)}
                    </div>
                </div>}

                {editable && <div
                    className="row no-gutters align-items-center mb-2">
                    <div
                        className="col-7">
                        <strong>
                            {t('form.labels.complete_sections')}:
                        </strong>
                    </div>
                    <div
                        className="col-5">
                        {completedSections.length} / {sections.length}
                    </div>
                </div>}

                {sections_list({
                    t,
                    sections,
                    openSection: this.props.openSection,
                    editable
                })}

                {editable && this.renderButtons()}

            </div>
        </PageLayout>;
    }

    renderButtons () {
        const { t, completedSections, sections } = this.props;
        const can_finalize = completedSections.length === sections.length;

        return <div
            className="clearfix mt-3">
            {this.props.attributes.id && <button
                className="btn btn-danger float-left"
                onClick={this.props.requestDeleteForm}>
                {t('form.actions.delete')}
            </button>}

            {can_finalize && <button
                className="btn btn-success float-right"
                onClick={this.finalizeForm}
                disabled={this.state.finalizeButtonDisabled}>
                {t('form.actions.finalize')}
            </button>}
        </div>;
    }

    finalizeForm(){
        this.setState({
            finalizeButtonDisabled: true,
        });
        this.props.finalizeForm();
    }

}

function state ({ form, sections }) {
    const { editable, attributes } = form;
    return {
        editable,
        sections,
        completedSections: sections.filter(s => s.complete),
        attributes,
    };
}

function dispatch (dispatch) {
    return {
        openSection (e) {
            dispatch({
                type: 'OPEN_FORM_SECTION',
                section: e.currentTarget.dataset.section
            });
        },

        leave () {
            ema.invokeAction('leaveForm');
        },

        confirmDeleteForm (form_id) {
            dispatch({
                type: 'SHOW_MODAL',
                modal: 'delete_form',
                props: {
                    id: form_id
                }
            });
        }
    };
}

function mergeProps (state, dispatch, own) {
    return {
        ...own,
        ...state,
        ...dispatch,

        requestDeleteForm () {
            dispatch.confirmDeleteForm(state.attributes.id);
        },

        finalizeForm () {
            ema.invokeAction(
                'finalizeFormAndLeave',
                state.attributes
            );
        }
    };
}

const ConnectedFormPage = connect(
    state,
    dispatch,
    mergeProps
)(
    FormPage
);
export default ConnectedFormPage;

function sections_list ({t, sections, openSection, editable}) {
    return <ul
        className="list-group mt-3">
        {sections.map(({name, complete}) =>
            <li
                key={name}
                className="list-group-item"
                onClick={openSection}
                data-section={name}>
                {t(`form.q.groups.${name}.abbr`)}
                {editable && complete &&
                <CheckIcon
                    className="badge badge-secondary float-right -icon"
                    fill="white"/>
                }
            </li>
        )}
    </ul>;
}
