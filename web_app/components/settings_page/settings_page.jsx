import React from 'react';
import {connect} from 'react-redux';
import ema from '../../ema';
import utils from '../../utils';

import PageLayout from '../page_layout';
import LogInForm from './login_form';
import CheckedIcon from '@fortawesome/fontawesome-free/svgs/regular/check-square.svg';
import NonCheckedIcon from '@fortawesome/fontawesome-free/svgs/regular/square.svg';

class SettingsPage extends React.Component {

    render () {
        const {t, user, returnFromSettings} = this.props;
        const top_bar = {
            t,
            caption: t('settings.caption'),
            goBack: returnFromSettings
        };
        return <PageLayout
            bar={top_bar}>
            <div
                className="ema-body">

                <ul
                    className="list-group">
                    {this.languageOption('en')}
                    {this.languageOption('pt')}
                </ul>

                <hr/>

                {
                    user.login ?
                        this.user() :
                        this.loginForm()
                }

            </div>
        </PageLayout>;
    }

    languageOption (for_lang) {
        const {t, onLangChange} = this.props;
        return <li
            className="list-group-item d-flex align-items-center"
            data-lang={for_lang}
            onClick={onLangChange}>
            <div>
                {React.createElement(( t.locale === for_lang ?
                        CheckedIcon :
                        NonCheckedIcon
                    ), {
                        className: 'ema-lang-check',
                        fill: '#9D9D9C'
                    }
                )}
            </div>
            <div
                className="ml-4">
                {utils.langName(for_lang)}
            </div>
        </li>;
    }

    loginForm () {
        const {t, login_fail, onLogInRequested} = this.props;
        return <LogInForm
            t={t}
            fail={login_fail}
            onSubmit={onLogInRequested}/>;
    }

    user () {
        const {t, user, requestLogOut, logout_fail} = this.props;
        return <div>
            <div
                className="alert alert-success">
                {t('user.logged_in_as')}: {user.name} ({user.login})
            </div>

            <div
                className="text-center">
                <button
                    className="btn btn-danger"
                    onClick={requestLogOut}>
                    {t('settings.log_out')}
                </button>

                {logout_fail && <div
                    className="alert alert-danger mt-3">
                    <strong>
                        {t(`app.native.fail.${logout_fail}`)}
                    </strong>
                </div>}
            </div>
        </div>;
    }

}

function state ({user, login_fail, logout_fail}) {
    return {
        user: (user || {}),
        login_fail,
        logout_fail
    };
}

function dispatch (dispatch) {
    return {
        returnFromSettings () {
            dispatch({
                type: 'GOTO_HOME_PAGE'
            });
        },

        onLangChange (e) {
            ema.invokeAction(
                'changeLanguage',
                e.currentTarget.dataset.lang
            );
        },

        onLogInRequested (data) {
            ema.invokeAction('logIn', data);
        },

        requestLogOut () {
            dispatch({
                type: 'SHOW_MODAL',
                modal: 'log_out'
            });
        }
    };
}

const ConnectedSettingsPage = connect(
    state,
    dispatch
)(
    SettingsPage
);
export default ConnectedSettingsPage;
