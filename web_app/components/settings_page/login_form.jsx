import React from 'react';

export default class LogInForm extends React.Component {

    constructor (props) {
        super(props);
        this.state = {
            login: '',
            password: ''
        };

        this.onLoginChange = e => {
            if (this.props.processing) return;
            this.setState({login: e.target.value});
        };

        this.onPasskeyChange = e => {
            if (this.props.processing) return;
            this.setState({password: e.target.value});
        };

        this.onSubmit = () => {
            this.props.onSubmit({
                login: this.state.login,
                password: this.state.password
            });
        };
    }

    render () {
        const {t, fail} = this.props;
        const {login, password} = this.state;
        const enabled = login.length > 0 && password.length > 0;
        return <div>
            <div
                className="form-group">
                <label
                    htmlFor="user_login">
                    {t('settings.user_login')}
                </label>
                <input
                    type="text"
                    id="user_login"
                    className="form-control"
                    value={login}
                    onChange={this.onLoginChange}/>
            </div>

            <div
                className="form-group">
                <label
                    htmlFor="user_passkey">
                    {t('settings.user_passkey')}
                </label>
                <input
                    type="password"
                    id="user_passkey"
                    className="form-control"
                    value={password}
                    onChange={this.onPasskeyChange}/>
            </div>

            {<div
                className="row no-gutters">
                <button
                    disabled={!enabled}
                    className="offset-3 col-6 btn btn-success"
                    onClick={this.onSubmit}>
                    {t('settings.log_in')}
                </button>
            </div>}

            {fail && <div
                className="alert alert-danger mt-3">
                <strong>
                    {t(`app.native.fail.${fail}`)}
                </strong>
            </div>}
        </div>;
    }

}
