import React from 'react';

import SettingsIcon from '@fortawesome/fontawesome-free/svgs/solid/cog.svg';
import BackIcon from '@fortawesome/fontawesome-free/svgs/solid/angle-up.svg';

export default class TopBar extends React.Component {

    render () {
        const {no_logo, caption, openSettings, goBack} = this.props;
        return <div
            className="row no-gutters align-items-center ema-bar">
            <div
                className="col-3 p-1 d-flex align-items-center">
                {!no_logo && <img
                    src={`build/top_logo.png`}
                    className="img-fluid"/>}
            </div>
            <div
                className="col-7 pl-2 -caption">
                {caption}
            </div>
            {openSettings && <button
                className="col-2 btn"
                onClick={openSettings}>
                <SettingsIcon
                    fill="white"/>
            </button>}
            {!openSettings && goBack && <button
                className="col-2 btn"
                onClick={goBack}>
                <BackIcon
                    fill="white"/>
            </button>}
        </div>;
    }

}
