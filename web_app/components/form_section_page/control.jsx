import React from 'react';
import { connect } from 'react-redux';
import ema from '../../ema';
import utils from '../../utils';
import * as icons from '../icons';

export default function Control (props) {
    const {t, id, type, control} = props;
    const component = componentForType(control || type);
    return <div
        className="form-group">
        <label
            htmlFor={id}>
            {t(`form.q.attrs.${id}`)}
        </label>
        {component && React.createElement(component, props)}
    </div>;
}

function componentForType (type) {
    switch (type) {
        case 'integer':
            return ConnectedIntegerInput;
        case 'number':
            return ConnectedNumericInput;

        case 'text':
            return ConnectedTextInput;
        case 'text-long':
            return ConnectedLongTextInput;

        case 'select':
            return ConnectedSelect;

        case 'yesno':
            return ConnectedYesNo;

        case 'gps':
            return ConnectedGpsLocation;
    }
}

function update_attr_action (id, value) {
    return {
        type: 'FORM_UPDATE_ATTR',
        id,
        value
    };
}

////////////////////////////////////////////////////////////////////////////////
/// Numeric Input
////////////////////////////////////////////////////////////////////////////////

class NumericInput extends React.Component {

    constructor (props) {
        super(props);
        this.state = {
            current_value: NumericInput.parseValue(props.value)
        };
        this.handleChange = this.handleChange.bind(this);
        this.onChange = this.onChange.bind(this);
    }

    componentDidUpdate (prevProps) {
        if (prevProps.value !== this.props.value) {
            const new_value = NumericInput.parseValue(this.props.value);
            this.setState({current_value: new_value});
        }
    }

    render () {
        return <input
            ref={input => this.input_element = input}
            id={this.props.id}
            type="text"
            className="form-control"
            value={this.state.current_value}
            onChange={this.handleChange}
            onBlur={this.onChange}/>;
    }

    handleChange (e) {
        this.setState({current_value: e.target.value});
    }

    onChange () {
        const new_value = this.props.normalizer(
            this.state.current_value.replace(',', '.').trim()
        );
        this.setState({current_value: NumericInput.parseValue(new_value)});
        this.props.onChange(new_value);
    }

    static parseValue (value) {
        if (typeof value !== 'number') return '';
        else return String(value);
    }

}

const ConnectedIntegerInput = connect(
    (state, props) => ({
        value: state.form.attributes[props.id],
        normalizer: value => {
            if (value === '') return null;
            value = Number(value);
            value = (isNaN(value) ?
                null :
                Math.floor(value));
            return value;
        }
    }),
    (dispatch, props) => ({
        onChange (value) {
            dispatch(update_attr_action(
                props.id,
                value
            ));
        }
    })
)(NumericInput);

const ConnectedNumericInput = connect(
    (state, props) => ({
        value: state.form.attributes[props.id],
        normalizer: value => {
            if (value === '') return null;
            value = Number(value);
            value = (isNaN(value) ?
                null :
                value);
            return value;
        }
    }),
    (dispatch, props) => ({
        onChange (value) {
            dispatch(update_attr_action(
                props.id,
                value
            ));
        }
    })
)(NumericInput);

////////////////////////////////////////////////////////////////////////////////
/// Text Input
////////////////////////////////////////////////////////////////////////////////

const ConnectedTextInput = connect(
    (state, props) => ({
        value: state.form.attributes[props.id]
    }),

    (dispatch, props) => ({
        onChange (e) {
            dispatch(update_attr_action(
                props.id,
                e.target.value
            ));
        }
    })
)(
    ({id, value, onChange}) => {
        return <input
            id={id}
            type="text"
            className="form-control"
            defaultValue={value}
            onBlur={onChange}/>;
    }
);

////////////////////////////////////////////////////////////////////////////////
/// Long Text Input
////////////////////////////////////////////////////////////////////////////////

const ConnectedLongTextInput = connect(
    (state, props) => ({
        value: state.form.attributes[props.id]
    }),

    (dispatch, props) => ({
        onChange (e) {
            dispatch(update_attr_action(
                props.id,
                e.target.value
            ));
        }
    })
)(
    ({id, value, onChange}) => {
        return <textarea
            id={id}
            className="form-control"
            defaultValue={value}
            onBlur={onChange}/>;
    }
);

////////////////////////////////////////////////////////////////////////////////
/// Select
////////////////////////////////////////////////////////////////////////////////

const ConnectedSelect = connect(
    ({ form, lang }, { id }) => {
        const value = form.attributes[id];
        if (value === undefined || value === null) return { text: '' };

        const translation = QUESTIONNAIRE.fields.getOptionText(
            id, form.attributes, lang,
            key => utils.t(`form.q.opts.${id}.${key}`, lang)
        );
        const text = typeof translation === 'string' ?
            translation :
            (translation ? (translation.caption || translation.text) : '');
        return { text };
    },

    (dispatch, props) => ({
        onToggleOptions () {
            dispatch({
                type: 'SHOW_MODAL',
                modal: 'select_option',
                props: {
                    field_id: props.id
                }
            });
        },

    })
)(
    ({id, text, onToggleOptions}) => {
        return <div
            className="input-group">
            <input
                id={id}
                type="text"
                disabled
                className="form-control"
                value={text}/>
            <div
                className="input-group-append">
                <button
                    className="btn btn-outline-secondary"
                    onClick={onToggleOptions}>
                    {React.createElement(
                        icons.CaretDownIcon,
                        { fill: '#9D9D9C' }
                    )}
                </button>
            </div>
        </div>;
    }
);

////////////////////////////////////////////////////////////////////////////////
/// YesNo toggle
////////////////////////////////////////////////////////////////////////////////

class YesNo extends React.Component {

    render () {
        const {t, value, onToggle} = this.props;
        return <div
            className="row no-gutters">
            <div
                className="col-3 d-flex"
                onClick={onToggle}
                data-val="1">
                <div
                    className="flex-grow-1">
                    {React.createElement(( value ?
                            icons.CheckedIcon :
                            icons.NonCheckedIcon
                        ), {
                            className: 'badge -icon',
                            fill: '#9D9D9C'
                        }
                    )}
                </div>
                <div
                    className="flex-grow-1">
                    {t('from.controls.yesno.yes')}
                </div>
            </div>
            <div
                className="offset-3 col-3 d-flex"
                onClick={onToggle}
                data-val="0">
                <div
                    className="flex-grow-1">
                    {React.createElement(( !value ?
                            icons.CheckedIcon :
                            icons.NonCheckedIcon
                        ), {
                            className: 'badge -icon',
                            fill: '#9D9D9C'
                        }
                    )}
                </div>
                <div
                    className="flex-grow-1">
                    {t('from.controls.yesno.no')}
                </div>
            </div>
        </div>;
    }

}

const ConnectedYesNo = connect(
    (state, props) => ({
        value: !!state.form.attributes[props.id]
    }),

    (dispatch, props) => ({
        onToggle (e) {
            dispatch(update_attr_action(
                props.id,
                (e.currentTarget.dataset.val === '1')
            ));
        }
    })
)(YesNo);

////////////////////////////////////////////////////////////////////////////////
/// GPS Location Input
////////////////////////////////////////////////////////////////////////////////

class GpsLocation extends React.Component {

    render () {
        const {t, id, value, control_state, onStart} = this.props;
        const text = value ?
            `lat: ${value.t}, lon: ${value.n}` :
            undefined;
        return <div
            className="ema-gps-input">
            <div
                className="input-group">
                <div
                    className="input-group-prepend">
                    <button
                        className="btn btn-outline-secondary"
                        onClick={onStart}>
                        <icons.LocationIcon
                            fill="#9D9D9C"
                        />
                    </button>
                </div>
                <input
                    id={id}
                    type="text"
                    disabled
                    className="form-control"
                    value={text}/>
            </div>

            {control_state && control_state.fail && <div
                className="alert alert-danger">
                {t(`app:native:fail:gps_${control_state.fail}`)}
            </div>}
        </div>;
    }

}

const ConnectedGpsLocation = connect(
    (state, props) => {
        const form = state.form;
        const control_state = form.controls && form.controls[props.id];
        return {
            value: state.form.attributes[props.id],
            control_state,
        };
    },

    (dispatch, props) => ({
        onStart () {
            ema.invokeAction('readGPS', props.id);
        }
    })
)(GpsLocation);
