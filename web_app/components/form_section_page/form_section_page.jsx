import React from 'react';
import {connect} from 'react-redux';

import PageLayout from '../page_layout';
import Control from './control';
import FieldValue from './field_value';
import Questionnaire from '../../reducers/structs/questionnaire';

class FormSectionPage extends React.Component {

    render() {
        const {t, name, leaveSection, editable} = this.props;
        const navigation = this.sectionNavigation(name);
        const top_bar = {
            t,
            caption: this.barCaption(t, navigation),
            goBack: leaveSection
        };
        return <PageLayout
            bar={top_bar}>
            <div
                className="ema-body">

                <h3>
                    {t(`form.q.groups.${name}.text`)}
                </h3>

                {editable ?
                    this.renderControls() :
                    this.renderFieldValues()
                }
                {this.renderButtons(navigation)}
            </div>
        </PageLayout>;
    }

    barCaption (t, { index, count }) {
        return `${t('form.caption')} ${index + 1}/${count}`;
    }

    renderControls () {
        const {t, attributes, controls, lang} = this.props;
        const to_show = controls.filter(c => Questionnaire.showField(c.id, attributes));
        return to_show.map(c => {
            const props = {
                t,
                key: c.id,
                ...c,
                value: attributes[c.id],
                lang
            };
            return React.createElement(Control, props);
        });
    }

    renderFieldValues () {
        const {t, attributes, controls, lang} = this.props;
        const to_show = controls.filter(c => Questionnaire.showField(c.id, attributes));
        return to_show.map(c => {
            const props = {
                t,
                key: c.id,
                ...c,
                value: attributes[c.id],
                attributes,
                lang
            };
            return React.createElement(FieldValue, props);
        });
    }

    renderButtons (navigation) {
        const {t, openSection, leaveSection, editable} = this.props;
        return <div
            className="clearfix">
            {(navigation.previous !== null) && <button
                className="btn btn-secondary float-left"
                onClick={openSection}
                data-section={navigation.previous}>
                {t('form.actions.previous_section')}
            </button>}
            {
                (navigation.next !== null) ? <button
                        className="btn btn-secondary float-right"
                        onClick={openSection}
                        data-section={navigation.next}>
                        {t('form.actions.next_section')}
                    </button> :
                    <button
                        className="btn btn-primary float-right"
                        onClick={leaveSection}>
                        {t('form.actions.index')}
                    </button>
            }
        </div>;
    }

    sectionNavigation (section_name) {
        const sections = Questionnaire.sections_list;
        const index = sections.indexOf(section_name);
        const next_index = index + 1;

        const previous = index > 0 ?
            sections[index - 1]:
            null;
        const next = next_index < sections.length ?
            sections[next_index] :
            null;

        return {
            index,
            previous,
            next,
            count: sections.length
        };
    }

}

function state ({ form, name, controls, lang }) {
    const {
        attributes,
        editable,
        return_action,
    } = form;
    return {
        attributes,
        name,
        controls,
        editable,
        return_action,
        lang,
    };
}

function dispatch (dispatch) {
    return {
        leaveSection () {
            dispatch({
                type: 'LEAVE_FORM_SECTION'
            });
        },

        openSection (e) {
            dispatch({
                type: 'OPEN_FORM_SECTION',
                section: e.currentTarget.dataset.section
            });
        }
    };
}

const ConnectedFormSectionPage = connect(
    state,
    dispatch
)(
    FormSectionPage
);
export default ConnectedFormSectionPage;
