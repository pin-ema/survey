import React from 'react';
import utils from '../../utils';
import * as icons from '../icons';

export default function FieldValue (props) {
    const {t, id, type, control} = props;
    const component = componentForType(control || type);
    return <div
        className="form-group">
        <label>
            {t(`form.q.attrs.${id}`)}
        </label>
        {component && React.createElement(component, props)}
    </div>;
}

function componentForType (type) {
    switch (type) {
        case 'integer':
        case 'number':
        case 'text':
        case 'text-long':
            return SimpleText;

        case 'gps':
            return GpsText;

        case 'select':
            return SelectedOption;

        case 'yesno':
            return YesNo;
    }
}

function SimpleText ({ value }) {
    if (value) return <div
        className="ema-field-value">
        { value }
    </div>;
    else return <div
        className="ema-field-value">
        &nbsp;
    </div>;
}

function GpsText ({ value }) {
    const text = value ?
        `lat: ${value.t}, lon: ${value.n}` :
        null;
    return SimpleText({ value: text });
}

function YesNo ({ t, value }) {
    return <div
        className="row no-gutters">
        <div
            className="col-3 d-flex">
            <div
                className="flex-grow-1">
                {React.createElement(( value ?
                        icons.CheckedIcon :
                        icons.NonCheckedIcon
                    ), {
                        className: 'badge -icon',
                        fill: '#9D9D9C'
                    }
                )}
            </div>
            <div
                className="flex-grow-1">
                {t('from.controls.yesno.yes')}
            </div>
        </div>
        <div
            className="offset-3 col-3 d-flex">
            <div
                className="flex-grow-1">
                {React.createElement(( !value ?
                        icons.CheckedIcon :
                        icons.NonCheckedIcon
                    ), {
                        className: 'badge -icon',
                        fill: '#9D9D9C'
                    }
                )}
            </div>
            <div
                className="flex-grow-1">
                {t('from.controls.yesno.no')}
            </div>
        </div>
    </div>;
}

function SelectedOption ({ id, value, attributes, lang }) {
    if (value) {
        const translation = QUESTIONNAIRE.fields.getOptionText(
            id, attributes, lang,
            key => utils.t(`form.q.opts.${id}.${key}`, lang)
        );
        value = typeof translation === 'string' ?
            translation :
            (translation ? (translation.caption || translation.text) : '');
    }
    return SimpleText({value});
}
