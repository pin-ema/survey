import utils from './utils';
import { generateRandomAttrs, fillOneAttr } from 'QUESTIONNAIRE_SRC/random';

const ema = {
    QUESTIONNAIRE,
    native: EMA_NATIVE,
    store: null,

    start (init_state) {
        // createData();

        const { persisted_state } = init_state;
        console.log(JSON.stringify({persisted_state}));

        switch (persisted_state && persisted_state.state_id) {
            case 'STATE:form_changed':
                ema.invokeAction('reopenChangedForm',
                    QUESTIONNAIRE.parseAttributes(persisted_state.attributes)
                );
                break;

            default:
                ema.invokeAction('goHome');

        }
    },

    stateToPersist () {
        const data = {};
        const state = ema.store.getState();

        switch (state.page_id) {
            case 'FormPage':
            case 'FormSectionPage':
                if (state.form.attrs_changed) {
                    data.state_id = 'STATE:form_changed';
                    data.attributes = QUESTIONNAIRE.serializeAttributes(
                        state.form.attributes
                    );
                }
                break;
        }

        return data;
    },

    navigateBack () {
        const ret = {};

        switch (ema.store.getState().page_id) {
            case 'HomePage':
                ret.ignored = '1';
                break;

            case 'FormPage':
                ema.invokeAction('leaveForm');
                break;

            case 'FormSectionPage':
                ema.store.dispatch({
                    type: 'LEAVE_FORM_SECTION'
                });
                break;

            default:
                ema.invokeAction('goHome');
                break;
        }

        return ret;
    },

    invokeAction(name, ...args) {
        try {
            const action = typeof name === 'function' ?
                name :
                ACTIONS[name];
            if (action) {
                return action.apply(null, args);
            } else {
                throw `unknown ema.actions.${name}`;
            }

        } catch (error) {
            console.log(`catched error: ${error.toString()}`);
            ema.store.dispatch({
                type: 'APP_FAILED',
                error: error
            });
            throw error;
        }
    },

};
///////////////////
///   ACTIONS   ///
///////////////////
const ACTIONS = {
    init () {
        const init_state = safeParse(
            ema.native.getInitState(),
            'init'
        );

        const lang = init_state ?
            init_state.language :
            'en';
        ema.store.dispatch({
            type: 'CHANGE_LANG',
            lang
        });

        if (init_state && init_state.user_login) {
            ema.store.dispatch({
                ...init_state,
                type: 'LOG_IN_SUCCESS',
            });
        }

        ema.start(init_state);
    },

    goHome () {
        ema.store.dispatch({
            type: 'GOTO_HOME_PAGE'
        });
        if (ema.store.getState().user) {
            ema.invokeAction('fetchUserData');
        }
    },

    fetchUserData () {
        const user_data = safeParse(
            ema.native.getUserData(),
            'user_data'
        );
        ema.store.dispatch({
            type: 'UPDATE_USER_DATA',
            data: user_data || {}
        });
    },

    changeLanguage (lang) {
        ema.native.setLocale(lang);
        ema.store.dispatch({
            type: 'CHANGE_LANG',
            lang: lang
        });
    },

    logIn (data) {
        ema.store.dispatch({
            type: 'NATIVE_STARTED',
            msg: 'app.native.logging_in'
        });

        waitForNative('logIn', return_data => {
            ema.store.dispatch({
                type: 'HIDE_MODAL'
            });

            if (return_data.ok) {
                ema.store.dispatch({
                    type: 'LOG_IN_SUCCESS',
                    ...return_data,
                });
                ema.invokeAction('goHome');

            } else {
                ema.store.dispatch({
                    type: 'LOG_IN_FAILED',
                    reason: (return_data.reason || 'server_down')
                });

            }
        });

        ema.native.logIn(JSON.stringify(data));
    },

    logOut () {
        ema.store.dispatch({
            type: 'NATIVE_STARTED',
            msg: 'app.native.logging_out'
        });

        waitForNative('logOut', return_data => {
            ema.store.dispatch({
                type: 'HIDE_MODAL'
            });

            if (return_data.ok) {
                ema.store.dispatch({
                    type: 'LOGGED_OUT'
                });
                ema.invokeAction('goHome');

            } else {
                ema.store.dispatch({
                    type: 'LOG_OUT_FAILED',
                    reason: (return_data.reason || 'server_down')
                });

            }
        });

        ema.native.logOut();
    },

    getFormAttrs (id) {
        const ret = ema.native.openForm(String(id));
        const form_data = safeParse(ret, 'getFormAttrs');
        return form_data === null ?
            null :
            QUESTIONNAIRE.parseAttributes(form_data);
    },

    saveForm (attrs) {
        attrs.updated_at = new Date();
        attrs = QUESTIONNAIRE.serializeAttributes(attrs);
        ema.native.saveForm(JSON.stringify(attrs));
        ema.invokeAction('fetchUserData');
    },

    deleteFormAndLeave (id) {
        id = String(id);
        ema.native.deleteForm(JSON.stringify({ id }));
        const store = ema.store;
        const { return_action } = store.getState().form;
        store.dispatch(return_action);
    },

    finalizeFormAndLeave (attrs) {
        attrs.finalized_at = new Date();
        attrs = QUESTIONNAIRE.serializeAttributes(attrs);
        ema.native.finalizeForm(JSON.stringify(attrs));
        ema.invokeAction('fetchUserData');
        const store = ema.store;
        const { return_action } = store.getState().form;
        store.dispatch(return_action);
    },

    reopenChangedForm (attributes) {
        ema.store.dispatch({
            type: 'OPEN_FORM',
            form: {
                editable: true,
                attrs_changed: true,
                attributes,
                return_action: {
                    type: 'GOTO_HOME_PAGE'
                },
            }
        });
    },

    leaveForm () {
        const store = ema.store;
        const { editable, attrs_changed, return_action } = store.getState().form;
        if (editable && attrs_changed) {
            store.dispatch({
                type: 'SHOW_MODAL',
                modal: 'leave_form',
                props: { return_action }
            });

        } else {
            store.dispatch(return_action);

        }
    },

    readGPS (id) {
        ema.store.dispatch({
            type: 'NATIVE_STARTED',
            msg: 'app.native.gps',
            process: 'gps'
        });

        waitForNative('readGPS', return_data => {
            ema.store.dispatch({
                type: 'HIDE_MODAL'
            });

            if (return_data.ok) {
                ema.store.dispatch({
                    type: 'FORM_UPDATE_ATTR',
                    id,
                    value: return_data.value
                });

            }
            else {
                ema.store.dispatch({
                    type: 'FORM_CONTROL_STATE',
                    id,
                    state: {
                        fail: return_data.reason || 'fail',
                    },
                });
            }
        });
        ema.native.readGPS();
    },

    cancelGps () {
        cancelNativeCallback();

        ema.native.cancelGps();

        ema.store.dispatch({
            type: 'HIDE_MODAL'
        });
    },

    uploadForms() {
        const user = ema.store.getState().user;
        if (!user || !user.data || !user.data.to_upload_count) return;

        ema.store.dispatch({
            type: 'NATIVE_STARTED',
            msg: 'app.native.uploading_forms'
        });

        waitForNative('uploadForms', return_data => {
            if (return_data.ok) {
                ema.invokeAction('fetchUserData');
                ema.store.dispatch({
                    type: 'SHOW_MODAL',
                    modal: 'info',
                    props: {
                        alert: 'success',
                        message: 'app.native.uploaded_successfully'
                    }
                });

            }
            else if (return_data.reason === 'unauthorized') {
                ema.store.dispatch({
                    type: 'HIDE_MODAL'
                });
                ema.invokeAction('kickUserOnRejectedToken');

            } else {
                const fail_msg = return_data.reason || 'server_down';
                ema.store.dispatch({
                    type: 'SHOW_MODAL',
                    modal: 'info',
                    props: {
                        alert: 'danger',
                        message: `app.native.fail.${fail_msg}`
                    }
                });

            }
        });
        ema.native.uploadFinalized();
    },

    kickUserOnRejectedToken () {
        ema.store.dispatch({
            type: 'LOGGED_OUT'
        });
        ema.store.dispatch({
            type: 'GOTO_SETTINGS_PAGE'
        });
        ema.store.dispatch({
            type: 'LOG_IN_FAILED',
            reason: 'bad_token'
        });
    }

};

function safeParse (json, label) {
    let result;
    try {
        result = JSON.parse(json);
    } catch (e) {
        console.error(`could not parse ${label}: ${json.toString()}`);
        return null;
    }
    return result;
}

const MIN_NATIVE_TIME = 400;
function waitForNative (label, fn) {
    const start = new Date();
    const callback = (payload) => {
        cancelNativeCallback();
        const data = safeParse(payload, label);
        if (data === null) return;

        const wait_for = MIN_NATIVE_TIME - (new Date() - start);
        if (wait_for > 0) {
            setTimeout(() => fn(data), wait_for);
        } else {
            fn(data);
        }
    };

    ema.native.callback = (payload) => {
        ema.invokeAction(callback, payload);
    };
}

function cancelNativeCallback () {
    delete ema.native.callback;
}

function createData () {
    // // create some forms for dev
    // (function () {
    //     for (let i=0; i<1; i+=1) {
    //         const attrs = generateRandomAttrs(QUESTIONNAIRE);
    //         ema.native.saveForm(JSON.stringify(attrs));
    //     }
    //
    //     for (let i=0; i<1; i+=1) {
    //         const attrs = generateRandomAttrs(QUESTIONNAIRE, {
    //             filter: field => field.group === 'meta' ? true : Math.random() > 0.3
    //         });
    //         ema.native.saveForm(JSON.stringify(attrs));
    //     }
    //
    //     for (let i=0; i<1; i+=1) {
    //         const attrs = generateRandomAttrs(QUESTIONNAIRE);
    //         for (const name of ['school_municipality', 'school_commune']) {
    //             if (attrs[name]) continue;
    //             attrs[name] = fillOneAttr(attrs, name, QUESTIONNAIRE);
    //         }
    //         attrs.finalized_at = Number(new Date());
    //         ema.native.finalizeForm(JSON.stringify(attrs));
    //     }
    // }());
}

self.EMA = ema;
export default ema;
