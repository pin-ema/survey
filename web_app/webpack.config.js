const webpack = require('webpack');
const path = require('path');
const fs = require('fs');

let env = process.env.NODE_ENV || 'development';
const dev = env === 'development';
const prod = env === 'production';

const babel_loader = {
    loader: 'babel-loader',
    options: {
        "presets": [
            [
                "@babel/preset-env",
                {
                    "targets": {
                        "android": "4.4"
                    },
                    "useBuiltIns": "entry",
                    "forceAllTransforms": true
                }
            ],
            "@babel/preset-react"
        ]
    }
};

const lib_path = path.resolve(__dirname, '../../oq_web/lib/questionnaire');
const definition_path = lib_path + '/plugin/src/ema_form.js';
if (!fs.existsSync(definition_path)) {
    const msg = [
        'Form definition not found in the directory:',
        definition_path
    ];
    throw msg.join("\n");
}

const provide = {
    QUESTIONNAIRE: [definition_path, 'default']
};

if (dev) {
    provide['EMA_NATIVE'] = ['./mock_ema_native', 'default'];
}

const plugins = [
    new webpack.ProvidePlugin(provide)
];

module.exports = {
    mode: prod ? 'production' : 'development',
    devtool: prod ? 'source-map' : 'eval-cheap-source-map',

    entry: './index.js',
    output: {
        filename: 'app.js',
        path: ( env === 'development' ?
                path.resolve(__dirname, './build') :
                path.resolve(__dirname, '../app/src/main/assets/build')
        )
    },

    resolve: {
        extensions: ['.jsx', '.js', '.json', '.scss'],
        alias: {
            jquery: 'jquery-slim',
            QUESTIONNAIRE_SRC: path.resolve(lib_path, 'src'),
            NODE_MODULES: path.resolve(__dirname, './node_modules'),
        }
    },

    module: {
        rules: [
            {test: /\.jsx?$/, exclude: /node_modules/, use: [babel_loader]},
            {
                test: /\.scss$/,
                use: [
                    'style-loader',
                    {loader: 'css-loader', options: {url: false}},
                    'sass-loader'
                ]
            },
            {test: /\.svg$/, use: [babel_loader,'react-svg-loader']},
            {test: /\.csv$/, use: ['csv-loader']},
        ]
    },

    plugins
};
