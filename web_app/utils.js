import uuid from 'uuid/v4';
import locales from './resources/locales';

const INIT_LOCALE = 'en';

const utils = {
    fTime (date) {
        if (!date) return '';
        const D = date.getDate(),
            M = date.getMonth() + 1,
            Y = date.getFullYear(),
            h = date.getHours(),
            m = date.getMinutes();

        return `${D}. ${M}. ${Y} ${h}:${m}`;
    },

    langName (lang) {
        switch (lang) {
            case 'en':
                return 'English';

            case 'pt':
                return 'Português';
        }
    },

    t (key, locale=INIT_LOCALE) {
        const value = translation_finder(locales[locale], key.split('.'), 0);
        return value || `Translation missing: >${key}<`;
    },

    debounce: function(func, wait, immediate) {
        var timeout, context, args;

        if (immediate) {
            return function () {
                var call_now = true;
                context = this;
                args = arguments;

                if (timeout) {
                    call_now = false;
                    clearTimeout(timeout);
                }
                timeout = setTimeout(function () { timeout = null; }, wait);
                if (call_now) func.apply(context, args);
            };

        } else {
            return function () {
                context = this;
                args = arguments;

                if (timeout) clearTimeout(timeout);

                timeout = setTimeout(function () {
                    timeout = null;
                    func.apply(context, args);
                }, wait);
            };
        }
    },

    uuid () {
        return uuid();
    },

    isObject (o) {
        return o !== null && typeof o === 'object';
    },

    arrayUniq (array) {
        return array.filter(
            (item, index, arr) => arr.indexOf(item) === index
        );
    }

};

function translation_finder (data, keys, i) {
    if (!utils.isObject(data)) return null;
    let value = data[keys[i]];
    i += 1;
    return i === keys.length ? value : translation_finder(value, keys, i);
}

export default utils;
