import "@babel/polyfill";

import ReactDOM from 'react-dom';
import App from './components/app';
import {createStore} from 'redux';
import appReducer from './reducers/reducers';
import ema from './ema';

import './assets/bundle';
import 'bootstrap/dist/js/bootstrap.bundle';
require('file-loader?name=font-reg.ttf!./assets/OpenSans-Regular.ttf');
require('file-loader?name=font-bold.ttf!./assets/OpenSans-Bold.ttf');
require('file-loader?name=pin.png!./assets/pin_logo.png');
require('file-loader?name=top_logo.png!./assets/ema.png');
require('file-loader?name=big_logo.png!./assets/ema_big.png');

const store = createStore(
    appReducer,
    (
        window &&
        window.__REDUX_DEVTOOLS_EXTENSION__ &&
        window.__REDUX_DEVTOOLS_EXTENSION__()
    )
);

ReactDOM.render(
    App.render_view(store),
    document.getElementById('app-container')
);

ema.store = store;
self.STORE = store;
ema.invokeAction('init');
