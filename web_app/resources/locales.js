import csv from './locales.csv';

const locales = {
    en: {},
    pt: {}
};

function getParentNode (root, keys) {
    let parent = root, child, i = 0, last = keys.length - 1, key;
    for (; i<last; i+=1) {
        key = keys[i];
        child = parent[key];
        if (!child) {
            child = {};
            parent[key] = child;
        }
        parent = child;
    }
    return parent;
}

// headers [key, en, pt]
csv.splice(0, 1);
csv.forEach(([key, en, pt]) => {
    if (!key) return;
    const keys = key.split('.');
    const last = keys[keys.length - 1];
    getParentNode(locales.en, keys)[last] = en;
    getParentNode(locales.pt, keys)[last] = pt;
});

// locales from from definition
['en', 'pt'].forEach(lang => {
   const form = locales[lang].form;
    locales[lang].form = {
        ...form,
        ...QUESTIONNAIRE.locales[lang]
    }
});

export default locales;
