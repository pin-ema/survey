import {action, getAction} from './actions_index';
import Questionnaire from './structs/questionnaire';
import utils from '../utils';

function emptyPageState ({ user, lang }, page_id) {
    return {
        page_id,
        user,
        lang
    }
}

self.APP_HISTORY = [];
APP_HISTORY.last = function () {
    return APP_HISTORY[APP_HISTORY.length - 1];
};

export default function appReducer (state={}, action) {
    const reducer = getAction(action.type);
    if (reducer !== null) {
        const next_state = reducer(state, action);
        if (self.APP_HISTORY !== undefined) {
            self.APP_HISTORY.push({
                action,
                state,
                next_state,
            });
        }
        return next_state;
    }
    else return state;
}

action('APP_FAILED', (state, { error }) => {
    return {
        ...state,
        error,
    };
});

////////////////////////////////////////////////////////////////////////////////
/// Settings & Language
////////////////////////////////////////////////////////////////////////////////

action('GOTO_SETTINGS_PAGE', state =>
    emptyPageState(state, 'SettingsPage')
);

action('CHANGE_LANG', (state, { lang }) => ({
    ...state,
    lang
}));

////////////////////////////////////////////////////////////////////////////////
/// User
////////////////////////////////////////////////////////////////////////////////

action('LOG_IN_SUCCESS', (state, user_data) => {
    const new_state = {
        ...state,
        user: {
            login: user_data.user_login,
            name: user_data.user_name,
            region: user_data.user_region,
            data: {}
        },
    };
    delete new_state.login_fail;
    delete new_state.logout_fail;
    return new_state;
});

action('LOG_IN_FAILED', (state, {reason}) => {
    return {
        ...state,
        login_fail: reason
    };
});

action('LOGGED_OUT', state => {
    const new_state = {
        ...state,
        user: null,
    };
    delete new_state.login_fail;
    delete new_state.logout_fail;
    return new_state;
});

action('LOG_OUT_FAILED', (state, { reason }) => {
    return {
        ...state,
        logout_fail: reason
    };
});

action('UPDATE_USER_DATA', (state, { data }) => {
    const parse = attrs => QUESTIONNAIRE.parseAttributes(attrs);
    const new_data = {
        finalized: (data.finalized || []).map(parse),
        unfinished: (data.unfinished || []).map(parse),
        to_upload_count: Number(data.to_upload_count || 0)
    };
    return {
        ...state,
        user: {
            ...state.user,
            data: new_data
        }
    };
});

////////////////////////////////////////////////////////////////////////////////
/// Modal
////////////////////////////////////////////////////////////////////////////////

action('NATIVE_STARTED', (state, {msg, process}) => {
    return {
        ...state,
        modal: {
            type: 'native',
            text: utils.t(msg, state.lang),
            process
        }
    }
});

action('SHOW_MODAL', (state, {modal, props}) => ({
    ...state,
    modal: {
        type: modal,
        ...props
    }
}));

action('HIDE_MODAL', state => ({
    ...state,
    modal: null
}));

////////////////////////////////////////////////////////////////////////////////
/// Forms Actions
////////////////////////////////////////////////////////////////////////////////

action('GOTO_HOME_PAGE', state =>
    emptyPageState(state, 'HomePage')
);

action('OPEN_FORM', (state, { form }) => {
    if (!form.attributes) form.attributes = Questionnaire.newAttributes(
        state.user
    );
    return {
        ...emptyPageState(state, 'FormPage'),
        form,
        sections: Questionnaire.sectionsList(form.attributes),
    };
});

action('OPEN_FORM_SECTION', (state, { section }) => {
    return {
        ...emptyPageState(state, 'FormSectionPage'),
        form: {
            ...state.form,
            controls: undefined,
        },
        name: section,
        controls: Questionnaire.controlsList(section),
    };
});

action('LEAVE_FORM_SECTION', state => {
    return getAction('OPEN_FORM')(
        state,
        {
            form: state.form
        }
    )
});

action('FORM_UPDATE_ATTR', (state, { id, value }) => {
    const current_form = state.form;
    const attributes = QUESTIONNAIRE.updateAttribute(
        current_form.attributes, id, value
    );
    if (current_form.attributes === attributes) return state;

    return {
        ...state,
        form: {
            ...current_form,
            attributes,
            attrs_changed: true,
        },
    };
});

action('FORM_CONTROL_STATE', (store_state, { id, state }) => {
    const form = store_state.form;
    let { controls } = form;
    controls = controls ? {
            ...controls,
            [id]: state,
        } :
        {
            [id]: state,
        };

    return {
        ...store_state,
        form: {
            ...form,
            controls,
        },
    };
});

action('GOTO_BROWSE_PAGE', (state, { list }) => {
    const page = 'BrowsePage';
    state = emptyPageState(state, page);
    return {
        ...state,
        list
    };
});
