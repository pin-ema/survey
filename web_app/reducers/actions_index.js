
const actions_index = {};

export function action (name, fn) {
    if (actions_index[name]) {
        throw `reducer action ${name} already exists`;
    }

    actions_index[name] = fn;
}

export function getAction (name) {
    const action = actions_index[name];
    return (typeof action === 'function') ?
        action :
        null;
}
