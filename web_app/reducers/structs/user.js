
export function generateThumbnail (attrs, lang) {
    const address = [
        'school_province',
        'school_municipality',
        'school_commune'
    ]
        .map(field => {
            return attrs[field] ?
                QUESTIONNAIRE.fields.getOptionText(
                    field, attrs, lang
                ) :
                ''
        })
        .join(', ');

    return {
        id: attrs.id,
        time: attrs.created_at,
        teacher: attrs.teacher_name,
        school: attrs.school_name,
        address: (address === ', , ' ?
            null :
            address)
    }
}
