const Questionnaire = {

    version: QUESTIONNAIRE.version,

    newAttributes ({ login, region }) {
        let attributes = {};
        // const defaults = QUESTIONNAIRE.defaultValues;
        // if (typeof defaults === 'function') attributes = defaults();
        // else if (typeof defaults === 'object') attributes = defaults;
        // else attributes = {};

        const [ province, municipality, commune ] = region;
        if (province) attributes.school_province = province;
        if (municipality) attributes.school_municipality = municipality;
        if (commune) attributes.school_commune = commune;

        attributes = {
            ...attributes,
            created_at: new Date(),
            user: login,
            version: Questionnaire.version
        };
        return attributes;
    },

    sectionsList (attributes) {
        return Questionnaire.sections_list.map(name => {
            const fields = Questionnaire.fieldsOfGroup(name);
            const valid_controls = fields.filter(
                f => f.isValueValid(attributes)
            );
            return {
                name,
                complete: valid_controls.length === fields.length
            }
        });
    },

    controlsList (section_name) {
        return Questionnaire.fieldsOfGroup(section_name).map(f => ({
            id: f.name,
            control: f.control.control,
            type: f.control.type,
        }));
    },

    sections_list: (function () {
        const list = Object.keys(QUESTIONNAIRE.fields.groups);
        list.splice(list.indexOf('meta'), 1);
        return list;
    }()),

    fieldsOfGroup (group) {
        return QUESTIONNAIRE.fields.groups[group];
    },

    showField (name, attributes) {
        return QUESTIONNAIRE.getField(name).shouldBeFilled(attributes);
    },

};

export default Questionnaire;
