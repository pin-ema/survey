import utils from './utils';
import { generateRandomAttrs, fillOneAttr } from 'QUESTIONNAIRE_SRC/random';

const SERVER = 'http://localhost:3000';
// const SERVER = 'https://beta.ema-monitoring.org';

class Form {
    constructor (finalized, attrs) {
        this.finalized = finalized;
        attrs.id = utils.uuid();
        this.attrs = attrs;
    }
}

Form.allForUser = function (user) {
    return Object.keys(mock_native.store)
        .map(id => mock_native.store[id])
        .filter(f => f.attrs.user === user);
};

const mock_native = {
    store: {},

    getLocale: () => 'en',
    user: null,

    getUserInitData () {
        return JSON.stringify(mock_native.buildUserPayload());
    },

    logIn (payload) {
        const {login,  password} = JSON.parse(payload);

        mock_native.postData(
            'login',
            {
                login,
                password
            },
            response => {
                let return_data = response;

                if (return_data.ok) {
                    const { login, name, token } = response;
                    mock_native.user = { login, name, token };
                    return_data = {
                        ok: true,
                        user: mock_native.buildUserPayload()
                    };
                }

                mock_native.callback(
                    JSON.stringify(return_data)
                );
            }
        );
    },

    logOut () {
        mock_native.postData(
            'logout',
            {},
            response => {
                if (response.ok) {
                    mock_native.user = null;
                }

                mock_native.callback(
                    JSON.stringify(response)
                );
            }
        );
    },

    setLocale () {},

    openForm (id) {
        const form = mock_native.store[id];
        if (form) return JSON.stringify(form.attrs);
        else return 'null';
    },

    saveForm (attrs_payload) {
        mock_native.createOrUpdate(
            JSON.parse(attrs_payload)
        );
        return JSON.stringify(mock_native.buildUserPayload());
    },

    deleteForm (attrs_payload) {
        const {id} = JSON.parse(attrs_payload);
        delete mock_native.store[id];
        return JSON.stringify(mock_native.buildUserPayload());
    },

    finalizeForm (attrs_payload) {
        const form = mock_native.createOrUpdate(
            JSON.parse(attrs_payload)
        );
        form.finalized = true;
        return JSON.stringify(mock_native.buildUserPayload());
    },

    uploadFinalized () {
        const forms = Form.allForUser(mock_native.user.login)
            .filter(f => f.finalized)
            .map(f => f.attrs);

        mock_native.postData(
            'upload',
            {
                forms
            },
            response => {
                if (response.ok) {
                    forms.forEach(({id}) => delete mock_native.store[id]);
                    mock_native.callback(
                        JSON.stringify({
                            ok: true,
                            user: mock_native.buildUserPayload()
                        })
                    );

                } else {
                    mock_native.callback(
                        JSON.stringify(response)
                    );

                }
            }
        );
    },

    readGPS () {
        mock_native.gps_timeout = setTimeout(() => {
            mock_native.gps_timeout = null;

            mock_native.callback(
                JSON.stringify({
                    ok: true,
                    value: {
                        t: 0.4567,
                        n: 0.4589
                    }
                })
            );
        }, 2000);
    },

    cancelGps () {
        if (mock_native.gps_timeout) clearTimeout(mock_native.gps_timeout);
        mock_native.gps_timeout = null;
    },

    createOrUpdate (attrs) {
        let form;

        if (!attrs.id) {
            form = new Form(false, attrs);
            mock_native.store[attrs.id] = form;

        } else {
            form = mock_native.store[attrs.id];
            form.attrs = attrs;

        }
        return form;
    },

    buildUserPayload () {
        const user = mock_native.user;
        if (!user) return null;

        const all_forms = Form.allForUser(user.login);
        const finalized = all_forms.filter(f => f.finalized);
        const unfinished = all_forms.filter(f => !f.finalized);
        return {
            login: user.login,
            name: user.name,
            finalized: finalized.map(f => ({...f.attrs})),
            unfinished: unfinished.map(f => ({...f.attrs})),
            to_upload_count: finalized.length
        };
    },

    async postData (action, data, callback) {
        data.token = mock_native.user && mock_native.user.token;
        let response_data;

        console.log('api POST', action, data);
        try {
            const response = await fetch(`${SERVER}/android_api/${action}`, {
                method: 'POST',
                body: JSON.stringify(data),
                headers: {'Content-Type': 'application/json'}
            });
            response_data = await response.json();

        }
        catch (e) {
            response_data = {
                ok: false
            };

        }

        callback(response_data);
    }
};

mock_native.__afterInit = function (ema) {
    const login = 'collector';

    for (let i=0; i<1; i+=1) {
        const attrs = generateRandomAttrs(QUESTIONNAIRE);
        const form = mock_native.createOrUpdate(attrs);
    }

    for (let i=0; i<5; i+=1) {
        const attrs = generateRandomAttrs(QUESTIONNAIRE, {
            filter: field => field.group === 'meta' ? true : Math.random() > 0.3
        });
        const form = mock_native.createOrUpdate(attrs);
    }

    for (let i=0; i<10; i+=1) {
        const attrs = generateRandomAttrs(QUESTIONNAIRE);
        for (const name of ['school_municipality', 'school_commune']) {
            if (attrs[name]) continue;
            attrs[name] = fillOneAttr(attrs, name, QUESTIONNAIRE);
        }
        attrs.finalized_at = Number(new Date());
        const form = mock_native.createOrUpdate(attrs);
        form.finalized = true;
    }

    ema.invokeAction('logIn', {
        login,
        password: '0Oo0Oo'
    });

    QUESTIONNAIRE.defaultValues = function () {
        const attrs = generateRandomAttrs(QUESTIONNAIRE, {
            filter: field => field.group === 'meta' ? false : Math.random() > 0.5
        });
        attrs.fe_method = 3;
        return attrs;
    };

};

export default mock_native;
